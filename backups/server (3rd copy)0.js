require('lookup-multicast-dns/global')
var topology = require('fully-connected-topology')
var jsonStream = require('duplex-json-stream')
var streamSet = require('stream-set')
var toPort = require('hash-to-port')
var register = require('register-multicast-dns')
var SHA256 = require("crypto-js/sha256");


var me = process.argv[2]
var peers = process.argv.slice(3)

var swarm = topology(pretvori(me), peers.map(pretvori))
var connections = streamSet()
var received = {}

register(me)

swarm.on('connection', function (prijatelj, id) {
  console.log('info> direct connection to', id)
  prijatelj = jsonStream(prijatelj);
  connections.add(prijatelj);

  var message=toJson(blockchain);
  console.log("\nSENDING MINE");

  var output = {from: id, seq: seq++, username: me, message: message}
  received[id] = seq // update my own so i dont retransmit

  connections.forEach(function (prijatelj) {
    prijatelj.write(output)
    console.log("   ...   ");
  })



  prijatelj.on('data', function (data) {
    console.log("\n[+] RECIVING DATA ....\n");

    if (data.seq <= received[data.from]) return // already received this one
    received[data.from] = data.seq

    console.log(data.username + '> ')

    var chain2=fromJson(data.message);
    if(isValidChain(chain2)){
      console.log("\nIS VALID CHECKING POWER\n");
      blockchain=chainCompare(blockchain,chain2);
    }
    diff_global=getLatestBlock().diff;
    outputChain(blockchain);

    connections.forEach(function (prijatelj) {
      prijatelj.write(data)
    })
  })


})

var seq = 0
var id = Math.random()

process.stdin.on('data', function (data) {
  console.log("\n[+] GENERATING DATA ....\n");
  //podatke pretvarjamo v blok blok dodam obstojecemu blockchainu in posljem kot json
  var temp_block = generateNextBlock(data.toString().trim());
  blockchain.push(temp_block);
  var message=toJson(blockchain);
  outputChain(blockchain);

  var output = {from: id, seq: seq++, username: me, message: message}
  received[id] = seq // update my own so i dont retransmit

  connections.forEach(function (prijatelj) {
    prijatelj.write(output)
  })
})

function pretvori (name) {
  return name + '.local:' + toPort(name)
}


//BLOCKCHAIN STUFF ////////////////////////////////////////////////////////////////////////////

var myVar = setInterval(myTimer, 20000); // for 5 min use 300000
var diff_global=1;
var stanje_prej=1;

function myTimer() {
  if(blockchain.length<=stanje_prej){
    if(diff_global>=2){
      diff_global--;
      console.log("\nDIFF--\n");
    }
  }else if(blockchain.length>=stanje_prej){
    if(diff_global<=4){
      diff_global++;
      console.log("\nDIFF++\n");
    }
  }

  console.log("\nCURENT STATE OF DIFF: "+diff_global+"    CHAIN LENGTH: "+ blockchain.length+"\n");

  stanje_prej=blockchain.length;
}




class Block {
  constructor(index, previousHash, timestamp, data, hash, diff, nonce) {
    this.index = index;
    this.timestamp = timestamp;
    this.data = data;
    this.hash = hash.toString();
    this.previousHash = previousHash.toString();
    this.diff = diff;
    this.nonce = nonce;
  }
}


function generateNextBlock(blockData) {
  var previousBlock = getLatestBlock();
  var nextIndex = previousBlock.index + 1;
  var nextTimestamp = new Date().getTime() / 1000;
  var diff = diff_global;
  var nonce = 0;

  while (true) {
    var nextHash = calculateHash(nextIndex, previousBlock.hash, nextTimestamp, blockData, diff, nonce);
    if (nextHash.startsWith(str_null(diff))) {
      return new Block(nextIndex, previousBlock.hash, nextTimestamp, blockData, nextHash, diff, nonce);
    } else {
      nonce++;
    }
  }

};

function calculateHash(index, previousHash, timestamp, data, diff, nonce) {
  return SHA256(index + previousHash + timestamp + data + diff + nonce).toString();
};

function calculateHashForBlock(block) {
  return SHA256(block.index + block.previousHash + block.timestamp + block.data + block.diff + block.nonce).toString();
}


function isValidNewBlock(newBlock, previousBlock) {
  if (previousBlock.index + 1 !== newBlock.index) {
    console.log('invalid index');
    return false;
  } else if (previousBlock.hash !== newBlock.previousHash) {
    console.log('invalid previoushash');
    return false;
  } else if (calculateHashForBlock(newBlock) !== newBlock.hash) {
    console.log('invalid hash: ' + calculateHashForBlock(newBlock) + ' ' + newBlock.hash);
    return false;
  }
  return true;
};


function str_null(num) {
  var i
  var str = "";
  for (i = 0; i < num; i++) {
    str = str + '0';
  }
  return str;
}


function getGenesisBlock() {

  return new Block(0, "0", 1547251266.938, "my genesis block!!", "0923a1cca5e6e9e7eaa9f2d76b5448479b66017c8b7082f259a03b18ec3aa0f1",1,22);

};

function outputBlock(block) {
  console.log(
    "\nBLOCK DATA //////////////////////\n" +
    "index: " + block.index + "\n" +
    "timestamp: " + block.timestamp + "\n" +
    "data: " + block.data + "\n" +
    "hash: " + block.hash + "\n" +
    "previousHash: " + block.previousHash + "\n" +
    "difficulty: " + block.diff + "\n" +
    "nonce: " + block.nonce + "\n" +
    "END OF BLOCK DATA ////////////////\n"

  );
}

function outputChain(chain){
  var i;
  for(i=0;i<chain.length;i++){
    outputBlock(chain[i]);
  }
  console.log("\nPOWER USED FOR CHAIN: "+calculatePower(chain));
}

function isValidChain(blockchainToValidate){
    if (JSON.stringify(blockchainToValidate[0]) !== JSON.stringify(getGenesisBlock())) {
        return false;
    }
    var tempBlocks = [blockchainToValidate[0]];
    for (var i = 1; i < blockchainToValidate.length; i++) {
        if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
            tempBlocks.push(blockchainToValidate[i]);
        } else {
            return false;
        }
    }
    return true;
};


function calculatePower(chain){
  var i;
  var pwr=0;
  for(i=0;i<chain.length;i++){
    pwr+=chain[i].nonce;
  }
  return pwr;
}

//vrne TRUE ce je druga "hisChain" dalsa!
function chainCompare(myChain, hisChain) {
  myPwr = calculatePower(myChain);
  hisPwr = calculatePower(hisChain);
  if (myChain.length < hisChain.length) {
    console.log("USING HIS");
    return hisChain;
  } else if (myChain.length > hisChain.length) {
    console.log("USING MINE");
    return myChain;
  } else {
    if (hisPwr > myPwr) {
      console.log("USING HIS");
      return hisChain;
    } else {
      console.log("USING MINE");
      return myChain;
    }

  }

}

function toJson(chain){
  return JSON.stringify(chain);
}



function fromJson(jsonString) {
  var obj = JSON.parse(jsonString);
  var i;
  var chain=[];
  for(i=0;i<obj.length;i++){
    chain.push(new Block(obj[i].index, obj[i].previousHash, obj[i].timestamp, obj[i].data, obj[i].hash, obj[i].diff, obj[i].nonce));
  }

  return chain;
}

function getLatestBlock(){
  return blockchain[blockchain.length - 1];
}

function getBefourBlock(){
  return blockchain[blockchain.length - 2];
}

var blockchain = [getGenesisBlock()];


//TESTS///////////////////////////////////////////////////
